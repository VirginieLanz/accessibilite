(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["index"],{

/***/ "./scripts/index.js":
/*!**************************!*\
  !*** ./scripts/index.js ***!
  \**************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);

jquery__WEBPACK_IMPORTED_MODULE_0___default()(function () {
  //Je déclare mes variables
  var btnCloseModal = jquery__WEBPACK_IMPORTED_MODULE_0___default()('.base-modal__close');
  var btnOpenModal = jquery__WEBPACK_IMPORTED_MODULE_0___default()('.base-modal__open');
  var modal = jquery__WEBPACK_IMPORTED_MODULE_0___default()('.base-modal'); //Je récupère tous mes éléments qui sont focusables dans la modal pour les distinguer de tous les autres éléments focusables du site

  var focusableElements = jquery__WEBPACK_IMPORTED_MODULE_0___default()('.base-modal__close[type="button"], .base-modal__body > textarea:not([disabled]), input[type="text"]:not([disabled]), input[type="submit"]:not([disabled]), input[type="checkbox"]:not([disabled]), select:not([disabled])'); //Je récupère le premier élément focusable de la modal pour le mettre dans une variable

  var firstFocusableElement = focusableElements[0]; //Je récupère le dernier élément focusable de la modal pour le mettre dans une variable

  var lastFocusableElement = focusableElements[focusableElements.length - 1]; //Comme le premier élément du tableau est le bouton de fermeture de la modal, je récupère le premier input du formulaire pour le mettre dans une variable

  var startFocus = focusableElements[1]; //Je fais une fonction qui est appelée lorsque la modal est ouverte. Il s'agit du Trap Focus

  function preventFocus() {
    //J'écoute tous les événements qui viennent du clavier de l'utilisateur
    modal.on('keydown', function (event) {
      //Si la touche Tab est tapée
      if (event.key == "Tab") {
        //Et si le focus actuel est positionné sur le dernier élément focusable de la modal 
        if (document.activeElement === lastFocusableElement) {
          //Et si la touche Tab est à nouveau tapée
          if (event.key == "Tab") {
            //Alors le premier élément de la modal récupère à nouveau le focus
            firstFocusableElement.focus();
            event.preventDefault();
          }
        }
      }
    });
  } //Fonction qui ouvre la modal 


  function openModal() {
    //Ajout de la classe qui affiche la modal
    modal.addClass('base-modal--show'); //Pointe le focus sur le premier input du formulaire présent dans la modal

    startFocus.focus(); //Appel du Trap Focus 

    preventFocus(); //Appel de la fonction closeModalByEsc pour quitter le modal autrement que par Tab

    closeModalByEsc();
  } //Fonction qui ferme la modal


  function closeModal() {
    //Retrait de la classe qui affiche la modal
    modal.removeClass('base-modal--show');
  } //Fonction qui ferme la modal en cliquant sur la touche Escape


  function closeModalByEsc() {
    //J'écoute tous les événements qui viennent du clavier de l'utilisateur
    jquery__WEBPACK_IMPORTED_MODULE_0___default()(document).on('keydown', function (event) {
      //Si la touche Escape est tapée
      if (event.key == "Escape") {
        //Alors la fonction closeModal est appelée
        closeModal();
      }
    });
  } //J'écoute les clics qui s'appliquent sur le bouton de fermeture du modal


  btnCloseModal.on('click', function (event) {
    //Si le bouton est cliqué, la fonction closeModal est appelée
    closeModal();
  }); //J'écoute les clics qui s'appliquent sur le bouton d'ouverte du modal

  btnOpenModal.on('click', function (event) {
    //Si le bouton est cliqué, la fonction openModal est appelée
    openModal();
  });
});

/***/ })

},[["./scripts/index.js","runtime","vendors~index"]]]);
//# sourceMappingURL=index.js.map